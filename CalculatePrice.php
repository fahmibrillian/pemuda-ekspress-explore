<?php
//Get from basic price setting on db
$basic_price = 4500;
$price_per_km = 1500;
$minimum_distance = 3;


$origin_lat  = '';
$origin_long = '';
$destination_lat = '';
$destination_long = '';


//TODO : Calculate distance from origin and destination pin point

$distance = 2 ; //

// if distance does not meet minimum distance, apply minimum_distance + 1

if($distance < $minimum_distance){
    $distance = $minimum_distance + 1;
}

$price = ($price_per_km * ($distance - $minimum_distance)) + $basic_price;

echo $price;