<?php

//Setting From DB
$minimal_area_search = 1; //Minimal Area

$gender_preference = 'F';

$origin_lat  = '';
$origin_long = '';
$destination_lat = '';
$destination_long = '';

$active_driver = [];
//TODO : find driver on area from database
//<editor-fold desc="Driver Sample">
$active_driver[] = [
    'name' => 'Usep Munandar',
    'gender' => 'M',
    'marriage' => true,
    'poin' => 14000,
    'distance_from_customer' => 0.5
    //TODO : determine how many orders to evenly each driver
];

$active_driver[] = [
    'name' => 'Ujang Sunarya',
    'gender' => 'M',
    'marriage' => true,
    'poin' => 12000,
    'distance_from_customer' => 1
];

$active_driver[] = [
    'name' => 'Susi Susanti',
    'gender' => 'F',
    'marriage' => false,
    'poin' => 12000,
    'distance_from_customer' => 0.6
];

//</editor-fold>

findDriver($active_driver,$minimal_area_search,$gender_preference);


function findDriver($active_driver,$minimal_area_search,$gender_preference){
    $selected_driver = $active_driver;
    $selected_driver = checkDriversInArea($selected_driver,$minimal_area_search);
    $selected_driver = checkGender($selected_driver,$gender_preference);
    $selected_driver = checkMarriage($selected_driver);

    if(count($selected_driver)<1){
        //if no driver is found, ignore the marriage variable
        $selected_driver = $active_driver;
        $selected_driver = checkDriversInArea($selected_driver,$minimal_area_search);
        $selected_driver = checkGender($selected_driver,$gender_preference);

        if(count($selected_driver)<1){
            //if no driver is found, ignore the gender variable
            $selected_driver = $active_driver;
            $selected_driver = checkDriversInArea($selected_driver,$minimal_area_search);

            if(count($selected_driver)<1) {
                //expand the area if no driver is found
                $minimal_area_search += 1;
                findDriver($active_driver, $minimal_area_search, $gender_preference);
            }
        }
    }

    if(count($selected_driver)>0) {
        $closest_driver = array_map(function($v){return $v['distance_from_customer'];}, $selected_driver);;
        $key = array_keys($closest_driver, min($closest_driver));
        print_r($selected_driver[$key[0]]);
    }
}


function checkDriversInArea($active_driver,$minimal_area_search){
    $filtered_driver = array_filter($active_driver, function ($var) use($minimal_area_search) {
        return ($var['distance_from_customer'] <=  $minimal_area_search);
    });

    return $filtered_driver;
}

function checkMarriage($active_driver){
    $filtered_driver = array_filter($active_driver, function ($var) {
        return ($var['marriage'] ==  true);
    });

    return $filtered_driver;
}

function checkGender($active_driver,$gender_preference){
    $filtered_driver = array_filter($active_driver, function ($var) use ($gender_preference){
        return ($var['gender'] ==  $gender_preference);
    });

    return $filtered_driver;
}

function checkDriverPoin(){
    //TODO: define how points work
}